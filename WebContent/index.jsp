<%@include file="init.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Book List</title>
</head>
<body>
<br/>
<a href="add.jsp" value="add">Add Book</a><br/><br/>
<%List<Book> bookList = (List<Book>)request.getAttribute("bookList"); %>
	<table border=1 cellspacing="5px" cellpadding="10px">
		<tr>
			<th>ISBN</th>
			<th>Title</th>
			<th>Price</th>
			<th>Description </th>
			<th></th>
			<th></th>
		</tr>
	<%for(Book book : bookList) {%>
		<tr>
			<td><%=book.getISBN()%></td>
			<td><%=book.getTitle()%></td>
			<td><%=book.getPrice()%></td>
			<td><%=book.getDescription()%> </td>
			<td><a href="BookShopServlet?action=edit&bookId=<%=book.getId()%>" id="edit">Edit</a></td>
			<td><a href="BookShopServlet?action=delete&bookId=<%=book.getId()%>" id="delete">Delete</a></td>
		</tr>
	<%} %>
	</table>
</body>
</html>

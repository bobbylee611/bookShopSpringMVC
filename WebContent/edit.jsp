<%@include file="init.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Book</title>
</head>
<body>
<% Book book = (Book)request.getAttribute("book"); %>
<form action="BookShopServlet?action=update" method="post">
ISBN:<input type="text" value="<%=book.getISBN()%>" name="ISBN"/> <br/>
Price:<input type="text" value="<%=book.getPrice()%>" name="price"/> <br/>
Title:<input type="text" value="<%=book.getTitle()%>" name="title"/> <br/>
<input type="hidden" value="<%=book.getId()%>" name="bookId"/>
Description:<textarea cols="100" rows="5" name="description"><%=book.getDescription()%></textarea> <br/>
<br/>
<input type="submit" value="submit"/>
</form>
</body>
</html>

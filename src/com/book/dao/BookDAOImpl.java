package com.book.dao;

import java.util.ArrayList;
import java.util.List;

import com.book.model.Book;

public class BookDAOImpl implements IBookDAO {

	List<Book> bookList = new ArrayList<Book>();

	public BookDAOImpl() {
		Book book1 = new Book(1, "978-1484218013-1", "Java Design Patterns-1", 23.21, "Learn how to implement design patterns in Java-1");
		Book book2 = new Book(2, "978-1484218013-2", "Java Design Patterns-2", 23.22, "Learn how to implement design patterns in Java-2");
		Book book3 = new Book(3, "978-1484218013-3", "Java Design Patterns-3", 23.23, "Learn how to implement design patterns in Java-3");

		this.bookList = new ArrayList<Book>();

		bookList.add(book1);
		bookList.add(book2);
		bookList.add(book3);
	}

	@Override
	public Book get(long id) {
		for(Book book : bookList) {
			if(book.getId() == id) {
				return book;
			}
		}
		return null;
	}

	@Override
	public void add(Book book) {
		bookList.add(book);
	}

	@Override
	public void update(Book updateBook) {
		for(Book book : bookList) {
			if(book.getId() == updateBook.getId()) {
				book = updateBook;
				break;
			}
		}
	}

	@Override
	public void delete(long id) {
		for(Book book : bookList) {
			if(book.getId() == id) {
				bookList.remove(book);
				break;
			}
		}
	}

	@Override
	public List<Book> getBookList() {
		return this.bookList;
	}

}

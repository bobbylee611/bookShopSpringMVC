package com.book.service;

import java.util.List;

import com.book.model.Book;

public interface IBookService {

	Book get(long id);

	void add(Book book);

	void update(Book updateBook);

	void delete(long id);

	public List<Book> getBookList();
}

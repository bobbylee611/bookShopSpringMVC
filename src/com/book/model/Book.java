package com.book.model;

public class Book {
	private long id;
	private String ISBN;
	private String title;
	private double price;
	private String description;

	public Book(long id, String iSBN, String title, double price, String description) {
		this.id = id;
		this.ISBN = iSBN;
		this.title = title;
		this.price = price;
		this.description = description;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		this.ISBN = iSBN;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", ISBN=" + ISBN + ", title=" + title + ", price=" + price + ", description=" + description + "]";
	}

}

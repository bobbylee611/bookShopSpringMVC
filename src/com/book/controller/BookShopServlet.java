package com.book.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.book.model.Book;
import com.book.service.BookServiceImpl;
import com.book.service.IBookService;

@WebServlet(description = "BookShopServlet", urlPatterns = { "/BookShopServlet" })
public class BookShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	IBookService bookServiceImpl = new BookServiceImpl();
	String VIEW = "index.jsp";

	public BookShopServlet() {
        super();
        System.out.println("BookShopServlet: " + this.hashCode());
    }

	public void init(ServletConfig config) throws ServletException {
		System.out.println("BookShopServlet initialized....");
	}

	public void destroy() {
		System.out.println("BookShopServlet destroyed");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action == null) {
			VIEW = "index.jsp";
			request.setAttribute("bookList", bookServiceImpl.getBookList());
			dispatcherRequest(request, response);
			return;
		}
		if(action.equals("add")) {
			String ISBN = request.getParameter("ISBN");
			String title = request.getParameter("title");
			double price = Double.parseDouble(request.getParameter("price"));
			String description = request.getParameter("description");
			long id = bookServiceImpl.getBookList().size() + 1;
			Book book = new Book(id, ISBN, title, price, description);
			bookServiceImpl.add(book);
			request.setAttribute("bookList", bookServiceImpl.getBookList());
			dispatcherRequest(request, response);
			return;
		}
		if("delete".equals(action)) {
			if(request.getParameter("bookId") != null) {
				long bookId = Long.parseLong(request.getParameter("bookId"));
				bookServiceImpl.delete(bookId);
				request.setAttribute("bookList", bookServiceImpl.getBookList());
				response.sendRedirect(request.getContextPath() + "/BookShopServlet");
			}
			return;
		}
		if("edit".equals(action)) {
			if(request.getParameter("bookId") != null) {
				long bookId = Long.parseLong(request.getParameter("bookId"));
				Book updateBook = bookServiceImpl.get(bookId);
				request.setAttribute("book", updateBook);
				VIEW = "edit.jsp";
				dispatcherRequest(request, response);
			}
			return;
		}
		if("update".equals(action)) {
			if(request.getParameter("bookId") != null) {
				long bookId = Long.parseLong(request.getParameter("bookId"));
				Book book = bookServiceImpl.get(bookId);
				book.setISBN(request.getParameter("ISBN"));
				book.setTitle(request.getParameter("title"));
				book.setPrice(Double.parseDouble(request.getParameter("price")));
				book.setDescription((request.getParameter("description")));
				response.sendRedirect(request.getContextPath() + "/BookShopServlet");
			}
		}
	}

	private void dispatcherRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher(VIEW);
		dispatcher.forward(request, response);
	}

}
